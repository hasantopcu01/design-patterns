package com.designpatterns.adapter.adaptee;

import com.designpatterns.adapter.models.Product;

import java.util.ArrayList;
import java.util.List;

public class Seller {

    private final List<Product> productList;

    public Seller() {
        productList = new ArrayList<>();
        Product smartPhone = new Product("SBrand", 1000, "USD");
        Product noteBook = new Product("NBrand", 2000, "USD");
        productList.add(smartPhone);
        productList.add(noteBook);
    }

    public List<Product> getProductList() {
        return productList;
    }

}
