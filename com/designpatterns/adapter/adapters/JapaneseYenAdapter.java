package com.designpatterns.adapter.adapters;


import com.designpatterns.adapter.models.Product;

import java.util.List;

public interface JapaneseYenAdapter {

    List<Product> productsJapaneseYenPrices();
}
