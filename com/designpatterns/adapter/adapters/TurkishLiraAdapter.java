package com.designpatterns.adapter.adapters;


import com.designpatterns.adapter.models.Product;

import java.util.List;

public interface TurkishLiraAdapter {

    List<Product> productsTurkishLiraPrices();
}
