package com.designpatterns.adapter.adapters.implementations;

import com.designpatterns.adapter.adaptee.Seller;
import com.designpatterns.adapter.adapters.EuroAdapter;
import com.designpatterns.adapter.models.Product;

import java.util.ArrayList;
import java.util.List;


public class EuroAdapterImplementation implements EuroAdapter {

   Seller seller = new Seller();

    private double usdEuroRate = 0.86;

    @Override
    public List<Product> productsEuroPrices() {
        System.out.println("Euro prices are calculating");
        List<Product> productEuroPrices = new ArrayList<>();
        for (Product sellerProduct : seller.getProductList()) {
            Product product = new Product();
            product.setProductName(sellerProduct.getProductName());
            product.setProductPriceUnit("EURO");
            product.setProductPrice(sellerProduct.getProductPrice() * usdEuroRate);
            productEuroPrices.add(product);
        }
        return productEuroPrices;
    }
}
