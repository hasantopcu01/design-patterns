package com.designpatterns.adapter.adapters.implementations;

import com.designpatterns.adapter.adaptee.Seller;
import com.designpatterns.adapter.adapters.JapaneseYenAdapter;
import com.designpatterns.adapter.models.Product;

import java.util.ArrayList;
import java.util.List;


public class JapaneseYenAdapterImplementation implements JapaneseYenAdapter {

    Seller seller = new Seller();

    private double usdYenRate = 113.40;

    @Override
    public List<Product> productsJapaneseYenPrices() {
        System.out.println("Japanese Yen prices are calculating");
        List<Product> productJapaneseYenPrices = new ArrayList<>();
        for (Product sellerProduct : seller.getProductList()) {
            Product product = new Product();
            product.setProductName(sellerProduct.getProductName());
            product.setProductPriceUnit("YEN");
            product.setProductPrice(sellerProduct.getProductPrice() * usdYenRate);
            productJapaneseYenPrices.add(product);
        }
        return productJapaneseYenPrices;
    }
}
