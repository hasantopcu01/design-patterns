package com.designpatterns.adapter.adapters.implementations;

import com.designpatterns.adapter.adaptee.Seller;
import com.designpatterns.adapter.adapters.TurkishLiraAdapter;
import com.designpatterns.adapter.models.Product;

import java.util.ArrayList;
import java.util.List;


public class TurkishLiraAdapterImplementation implements TurkishLiraAdapter {

    Seller seller = new Seller();

    private double usdTurkishLiraRate = 9.69;

    @Override
    public List<Product> productsTurkishLiraPrices() {
        System.out.println("Turkish lira prices are calculating");
        List<Product> productTurkishLiraPrices = new ArrayList<>();
        for (Product sellerProduct : seller.getProductList()) {
            Product product = new Product();
            product.setProductName(sellerProduct.getProductName());
            product.setProductPriceUnit("TL");
            product.setProductPrice(sellerProduct.getProductPrice() * usdTurkishLiraRate);
            productTurkishLiraPrices.add(product);
        }
        return productTurkishLiraPrices;
    }
}
