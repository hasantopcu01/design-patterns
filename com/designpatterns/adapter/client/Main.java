package com.designpatterns.adapter.client;

import com.designpatterns.adapter.adapters.EuroAdapter;
import com.designpatterns.adapter.adapters.JapaneseYenAdapter;
import com.designpatterns.adapter.adapters.TurkishLiraAdapter;
import com.designpatterns.adapter.adapters.implementations.EuroAdapterImplementation;
import com.designpatterns.adapter.adapters.implementations.JapaneseYenAdapterImplementation;
import com.designpatterns.adapter.adapters.implementations.TurkishLiraAdapterImplementation;
import com.designpatterns.adapter.models.Product;

import java.util.List;

public class Main {

    public static void main(String[] args) {
        EuroAdapter euroAdapter = new EuroAdapterImplementation();
        TurkishLiraAdapter turkishLiraAdapter = new TurkishLiraAdapterImplementation();
        JapaneseYenAdapter japaneseYenAdapter = new JapaneseYenAdapterImplementation();

        List<Product> productEuroPrices = euroAdapter.productsEuroPrices();
        for (Product productEuro : productEuroPrices) {
            System.out.println(productEuro.getProductName() + " " + productEuro.getProductPrice() + " " + productEuro.getProductPriceUnit());
        }


        List<Product> productTurkishLiraPrices = turkishLiraAdapter.productsTurkishLiraPrices();
        for (Product productTL : productTurkishLiraPrices) {
            System.out.println(productTL.getProductName() + " " + productTL.getProductPrice() + " " + productTL.getProductPriceUnit());
        }


        List<Product> productJapaneseYenPrices = japaneseYenAdapter.productsJapaneseYenPrices();
        for (Product productYEN : productJapaneseYenPrices) {
            System.out.println(productYEN.getProductName() + " " + productYEN.getProductPrice() + " " + productYEN.getProductPriceUnit());
        }
    }
}
