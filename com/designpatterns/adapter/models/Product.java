package com.designpatterns.adapter.models;

public class Product {
    private String productName;
    private double productPrice;
    private String productPriceUnit;


    public Product() { }

    public Product(String productName, double productPrice, String productPriceUnit) {
        this.productName = productName;
        this.productPrice = productPrice;
        this.productPriceUnit = productPriceUnit;

    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public double getProductPrice() {
        return productPrice;
    }

    public void setProductPrice(double productPrice) {
        this.productPrice = productPrice;
    }

    public String getProductPriceUnit() {
        return productPriceUnit;
    }

    public void setProductPriceUnit(String productPriceUnit) {
        this.productPriceUnit = productPriceUnit;
    }
}
