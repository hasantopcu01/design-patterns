package com.designpatterns.bridge.abstractions;

import com.designpatterns.bridge.bridge.Chair;
import com.designpatterns.bridge.bridge.ColorBridge;

public class XBrandModelA extends Chair {

    public XBrandModelA(ColorBridge colorBridge) {
        super(colorBridge);
    }

    @Override
    public void performPainting() {
        System.out.print("XBrandModelA ");
        colorBridge.paint();
    }
}
