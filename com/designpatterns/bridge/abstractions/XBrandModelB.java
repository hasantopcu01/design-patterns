package com.designpatterns.bridge.abstractions;

import com.designpatterns.bridge.bridge.Chair;
import com.designpatterns.bridge.bridge.ColorBridge;

public class XBrandModelB extends Chair {

    public XBrandModelB(ColorBridge colorBridge) {
        super(colorBridge);
    }

    @Override
    public void performPainting() {
        System.out.print("XBrandModelB ");
        colorBridge.paint();
    }
}
