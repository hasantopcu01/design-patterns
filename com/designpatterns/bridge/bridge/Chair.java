package com.designpatterns.bridge.bridge;


public abstract class Chair {

    protected ColorBridge colorBridge;

    public Chair() { }

    public Chair(ColorBridge colorBridge) {
        this.colorBridge = colorBridge;
    }

    public abstract void performPainting();
}
