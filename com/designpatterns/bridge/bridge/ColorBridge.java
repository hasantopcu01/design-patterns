package com.designpatterns.bridge.bridge;

public interface ColorBridge {

    void paint();
}
