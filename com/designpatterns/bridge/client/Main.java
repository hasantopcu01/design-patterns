package com.designpatterns.bridge.client;

import com.designpatterns.bridge.abstractions.XBrandModelA;
import com.designpatterns.bridge.abstractions.XBrandModelB;
import com.designpatterns.bridge.bridge.Chair;
import com.designpatterns.bridge.implementations.PaintBlue;
import com.designpatterns.bridge.implementations.PaintRed;

public class Main {
    public static void main(String[] args) {
        Chair redProductModelA = new XBrandModelA(new PaintRed());
        Chair blueProductModelA = new XBrandModelA(new PaintBlue());
        Chair blueProductModelB = new XBrandModelB(new PaintBlue());
        redProductModelA.performPainting();
        blueProductModelA.performPainting();
        blueProductModelB.performPainting();

    }
}
