package com.designpatterns.bridge.implementations;

import com.designpatterns.bridge.bridge.ColorBridge;

public class PaintBlue implements ColorBridge {

    @Override
    public void paint() {
        System.out.println("painted blue");
    }
}
