package com.designpatterns.bridge.implementations;

import com.designpatterns.bridge.bridge.ColorBridge;

public class PaintRed implements ColorBridge {

    @Override
    public void paint() {
        System.out.println("painted red");
    }
}
