package com.designpatterns.composite.client;

import com.designpatterns.composite.composite.Team;
import com.designpatterns.composite.leaf.BusinessAnalyst;
import com.designpatterns.composite.leaf.SoftwareDeveloper;
import com.designpatterns.composite.leaf.SolutionArchitect;
import com.designpatterns.composite.leaf.Tester;
import com.designpatterns.composite.shared.PriceCalculator;

public class Client {
    public static void main(String[] args) {

        Team developerTeam = new Team();
        SoftwareDeveloper developerAlyson = new SoftwareDeveloper("Alyson", "Software Developer", 1300);
        SoftwareDeveloper developerJoshua = new SoftwareDeveloper("Joshua", "Senior Software Developer", 1800);
        developerTeam.addEmployee(developerAlyson);
        developerTeam.addEmployee(developerJoshua);

        Team testerTeam = new Team();
        Tester testerKyle = new Tester("Kyle", "Quality Assurance Specialist", 1000);
        Tester testerBrad = new Tester("Brad", "Senior Quality Assurance Specialist", 1600);
        testerTeam.addEmployee(testerKyle);
        testerTeam.addEmployee(testerBrad);

        Team solutionArchitectTeam = new Team();
        SolutionArchitect solutionArchitectKim = new SolutionArchitect("Kim", "Solution Architect", 2000);
        SolutionArchitect solutionArchitectBen = new SolutionArchitect("Ben", "Senior Solution Architect", 3000);
        solutionArchitectTeam.addEmployee(solutionArchitectKim);
        solutionArchitectTeam.addEmployee(solutionArchitectBen);

        Team businessAnalystTeam = new Team();
        BusinessAnalyst businessAnalystEmily = new BusinessAnalyst("Emily", "Business Analyst", 1200);
        BusinessAnalyst businessAnalystKobe = new BusinessAnalyst("Kobe", "Senior Business Analyst", 2200);
        businessAnalystTeam.addEmployee(businessAnalystEmily);
        businessAnalystTeam.addEmployee(businessAnalystKobe);

        Team wholeTeams = new Team();
        wholeTeams.addEmployee(developerTeam);
        wholeTeams.addEmployee(testerTeam);
        wholeTeams.addEmployee(solutionArchitectTeam);
        wholeTeams.addEmployee(businessAnalystTeam);

        wholeTeams.calculateEmployeeSalary();

        PriceCalculator priceCalculator = PriceCalculator.getInstance();
        System.out.println("Total salary : " + priceCalculator.getTotalSalary());
        System.out.println("Software developer team total salary : " + priceCalculator.softwareDeveloperTotalSalary());
        System.out.println("Test team total salary : " + priceCalculator.testerTotalSalary());
        System.out.println("Solution architect team total salary : " + priceCalculator.solutionArchitectTotalSalary());
        System.out.println("Business analyst team total salary : " + priceCalculator.getBusinessAnalystTotalSalary());


    }
}
