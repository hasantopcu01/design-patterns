package com.designpatterns.composite.component;

public interface Employee {

    void calculateEmployeeSalary();
}
