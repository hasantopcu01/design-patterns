package com.designpatterns.composite.composite;

import com.designpatterns.composite.component.Employee;

import java.util.ArrayList;
import java.util.List;

public class Team implements Employee {

    List<Employee> employeeList;

    public Team() {
        this.employeeList = new ArrayList<>();
    }

    public void addEmployee(Employee employee) {
        employeeList.add(employee);
    }

    @Override
    public void calculateEmployeeSalary() {
        for (Employee employee : employeeList) {
            employee.calculateEmployeeSalary();
        }
    }
}
