package com.designpatterns.composite.leaf;

import com.designpatterns.composite.component.Employee;
import com.designpatterns.composite.shared.PriceCalculator;

public class SolutionArchitect implements Employee {

    private String name;
    private String position;
    private double salary;
    private PriceCalculator priceCalculator = PriceCalculator.getInstance();

    public SolutionArchitect(String name, String position, double salary) {
        this.name = name;
        this.position = position;
        this.salary = salary;
    }

    @Override
    public void calculateEmployeeSalary() {
        priceCalculator.addToSolutionArchitect(salary);
    }
}
