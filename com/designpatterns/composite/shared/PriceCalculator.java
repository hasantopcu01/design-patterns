package com.designpatterns.composite.shared;

public class PriceCalculator {

    private double businessAnalystTotalSalary = 0;
    private double solutionArchitectTotalSalary = 0;
    private double softwareDeveloperTotalSalary = 0;
    private double testerTotalSalary = 0;
    private double totalSalary = 0;

    private static PriceCalculator priceCalculator;

    private PriceCalculator() {
    }

    public static PriceCalculator getInstance() {
        if (priceCalculator == null) {
            priceCalculator = new PriceCalculator();
        }
        return priceCalculator;
    }

    public void addToBusinessAnalyst(double salary) {
        businessAnalystTotalSalary = businessAnalystTotalSalary + salary;
        totalSalary = totalSalary + salary;
    }

    public void addToSolutionArchitect(double salary) {
        solutionArchitectTotalSalary = solutionArchitectTotalSalary + salary;
        totalSalary = totalSalary + salary;
    }

    public void addToSoftwareDeveloper(double salary) {
        softwareDeveloperTotalSalary = softwareDeveloperTotalSalary + salary;
        totalSalary = totalSalary + salary;
    }

    public void addToTester(double salary) {
        testerTotalSalary = testerTotalSalary + salary;
        totalSalary = totalSalary + salary;
    }

    public double getBusinessAnalystTotalSalary() {
        return businessAnalystTotalSalary;
    }

    public double solutionArchitectTotalSalary() {
        return solutionArchitectTotalSalary;
    }

    public double softwareDeveloperTotalSalary() {
        return softwareDeveloperTotalSalary;
    }

    public double testerTotalSalary() {
        return testerTotalSalary;
    }

    public double getTotalSalary() {
        return totalSalary;
    }
}
